const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');
const socketio = require('socket.io');

const app = express();
const router = require('./routes/router');
const channel_router = require('./routes/channel');
const Users = require('./controllers/users');
const Database = require('./controllers/database');
const socketIOEvents = require('./socketIOEvents');

const cors = require('cors')// Db setup
const promise = mongoose.connect('mongodb://localhost:chat/chat');
Database.preconf(promise);

// App setup
app.use(cors());
app.use(morgan('combined'));
app.use(bodyParser.json({type: '*/*'}))

// routers
router(app);
channel_router(app);

// Server setup
const port = process.env.PORT || 3090;
const server = http.createServer(app);
const io = socketio(server);

let users = [];
socketIOEvents(io, users);

server.listen(port);
console.log('Server listening on port: ', port);

module.exports = app; // for testing

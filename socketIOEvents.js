const Channel = require('./models/channel');
const Users = require('./controllers/users');

module.exports = function (io, users) {
  io.on('connection', function(socket){
    console.log('a user connected', socket.id);
    var userName;

    // this shouldn't be here
    // and there should be error handling so the user will know that he haven't joined any room
    Channel.find({ name: 'trash' }, function (err, docs) {
      if (err) {
        console.log('Channel find error');
      }
      if (docs[0]) {
        socket.join(docs[0]._id);
      } else {
        socket.join('trash'); // not sure how to handle this
      }
    })

    socket.on('new_user', (username) => {
      userName = username;
      users.push({ username, id: socket.id });
      io.to(socket.id).emit('users_list', users);
      socket.broadcast.emit('new_user_bc', { username, id: socket.id });
    });

    socket.on('new_msg', (e) => {
      io.in(e.channelID).emit('msg', e);
    });

    socket.on('typing', (e) => {
      socket.broadcast.emit('typing_bc', userName);
    });

    socket.on('stop_typing', (e) => {
      socket.broadcast.emit('stop_typing_bc', userName);
    });

    socket.on('subscribe', function(id) {
      socket.join(id);
    })

    socket.on('unsubscribe', function(id) {
      socket.leave(id);
    })

    socket.on('new_channel', (channel) => {
      socket.broadcast.emit('new_channel_bc', channel)
    });

    socket.on('new_priv_channel', function(data) {
      socket.broadcast.to(data.socketID).emit('new_priv_channel_bc', data.channel);
    });

    socket.on('disconnect', (e) => {
      users = users.filter( e => e.username != userName );
      Users.removeUser(userName);
      io.emit('users_list', users);
    });
  });
}

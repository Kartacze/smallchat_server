const Channel = require('../models/channel');

const defaultChannel = "trash";

exports.preconf = function (mongoose) {
  mongoose.connection.on('connected', function () {
    var promise = Channel.find({ name: defaultChannel }, function (err, docs) {
      if (err) {
        console.log('Channel find error');
      }
      if (docs[0]) {
        console.log('trash channel is already in database');
      } else {
        const newChannel = new Channel({
          name: defaultChannel,
          private: false,
          between: [],
        });
        newChannel.save(function (err, data) {
          if (err) {
            console.log('Error while creating trash channel', err);
          }
          console.log('created trash channel');
        })
      }
    })
  });
};

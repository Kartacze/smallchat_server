
const User = require('../models/user');

exports.newUser = (req, res, next) => {
  const { username } = req.body;
  User.findOne({ 'username': req.body.username }, (err, doc) => {
    if (err) {
      console.log('error when adding new user', err);
      return res.status(500).json({msg: 'internal server error'});
    }
    if (doc) {
      // user exists
      res.json({error : "user already exists"});
    } else {
      // user do not exists
      const user = new User({
        username,
      });

      user.save(err => {
        if (err) { return next(err) }
        return res.json({response : "user registered properly", username });
      });
    }
  });
}

exports.removeUser = username => {
  console.log('username test', username);
  User.findOne( { username }, function(err, doc) {
    if(err) {return console.log('error while removing user', err);}
    console.log('doc found: ', doc);
    if(doc) {
      doc.remove();
    }
  })
}

const Channel = require('../models/channel');

// if there is a check of the private channel probably it should be also checked if the users are in database
// or it shouldn't been checked at all, just add the requesing user
exports.newChannel = (req, res, next) => {
  if ( !req.body.private || (req.body.private && req.body.between.length > 1)) {
    const newChannel = new Channel(req.body);
    newChannel.save((err, doc) => {
      if(err) {
        console.log('ERROR /channels', err);
        return res.status(500).json({msg: 'internal server error'});
      }
      res.json(doc);
    });
  } else {
    return res.status(500).json({msg: 'private channel should have min 2 users'});
  }
};

exports.getChannels = (req, res, next) => {
  console.log('name of get channels controllers', req.params.name);
  // { $or: [ { between: req.params.name }, { private: false } ] }
  Channel.find({ private: false },
    { name: 1, private: 1, between: 1, _id:1 },
    (err, docs) => {
    if(err) {
      console.log('ERROR /channels', err);
      return res.status(500).json({msg: 'internal server error'});
    }
    console.log('return', docs);
    res.json(docs);
  })
};

const mongoose = require("mongoose");
const Channel = require('../models/channel'); // w ogole nie potrzebne

//Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');
const should = chai.should();

chai.use(chaiHttp);
//Our parent block
describe('Channels', () => {

  describe('/channels POST channels', () => {
    const newChannel = {
      name: "test-name",
      private: false,
      between: [],
    }
    after(function() {
      Channel.remove({ name: newChannel.name }, function (err, done) {
        if (err) console.log('error when removing test channel', err);
        done();
      })
    });
    it('POST not private channel', (done) => {
      chai.request(server)
        .post('/channels')
        .type('Application/json')
        .send(newChannel)
        .end((err, res) => {
          console.log(res.body);
          res.should.have.status(200);
          // res.body.should.be.a('array');
          // res.body.length.should.be.eql(0);
          done();
        });
    });
  });

  // describe('/channels GET channels', () => {
  //     it('it should GET all the channels', (done) => {
  //       chai.request(server)
  //           .get('/book')
  //           .end((err, res) => {
  //               res.should.have.status(200);
  //               res.body.should.be.a('array');
  //               res.body.length.should.be.eql(0);
  //             done();
  //           });
  //     });
  // });

});

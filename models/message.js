const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define model
const MessageSchema = new Schema({
  channel: { type: String, unique: true },
  text: String,
  user: Object,
  time: String
});

module.exports = mongoose.model('message', MessageSchema);

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ChannelSchema = Schema({
  name: { type:String, unique: true },
  private: Boolean,
  between: Array
});

module.exports = mongoose.model('channel', ChannelSchema);

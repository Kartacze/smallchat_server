const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define model
const UserSchema = new Schema({
    username: { type: String, unique: true },
})

module.exports = mongoose.model('user', UserSchema);

const Channels = require('../controllers/channels');

module.exports = function (app) {
    app.get('/channels', Channels.getChannels);
    app.post('/channels', Channels.newChannel);
}
